#!/bin/sh
set -e
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/kernel.elf isodir/boot/kernel.elf
cp mktool/stage2_eltorito isodir/boot/grub
cat > isodir/boot/grub/menu.lst << EOF
default 0
title Boot PuckOS
kernel /boot/kernel.elf
EOF
genisoimage -R -b boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 4 -boot-info-table -o puckos.iso isodir
