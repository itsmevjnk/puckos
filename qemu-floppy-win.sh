#!/bin/sh
set -e
. ./floppy-win.sh

qemu-system-$(./target-triplet-to-arch.sh $HOST) -fda floppy.img -serial stdio
