/*
	acpi.c - ACPI driver
	<C> Copyright 2018 Weedboi6969@RTEA
*/

#include <kernel/acpi.h>
#include <kernel/paging86.h>
#include <kernel/inlineasm.h>
#include <kernel/uart.h>
#include <string.h>
#include <stdio.h>

acpi_rsdp_t *acpi_rsdp = NULL;
acpi_rsdt_t *acpi_rsdt = NULL;
acpi_xsdt_t *acpi_xsdt = NULL;
acpi_fadt_t *acpi_fadt = NULL;

static acpi_rsdp_t *acpi_get_rsdp(void) {
	if(acpi_rsdp != NULL) return acpi_rsdp;
	/* find from EBDA */
	uint16_t *t = 0x040E;
	uint16_t ebda = *t;
	ebda <<= 4;
	for(uint16_t i = 0; i < 1024; i++) {
		if(!strncmp((ebda + i), "RSD PTR ", 8)) {
			acpi_rsdp = (acpi_rsdp_t*) (ebda + i);
			return acpi_rsdp;
		}
	}
	
	/* cannot find from EBDA, find in the main BIOS area below 1MB */
	for(uint32_t i = 0xE0000; i < 0x100000; i++) {
		if(!strncmp(i, "RSD PTR ", 8)) {
			acpi_rsdp = (acpi_rsdp_t*) i;
			return acpi_rsdp;
		}
	}
	
	/* cannot find RSDP, return null */
	acpi_rsdp = NULL;
	return NULL;
}

static void *acpi_get_rsdt(void) {
	/* returns rsdt_address if ACPI version is 1.0 or xsdt_address if ACPI version is 2.0+ */
	if(acpi_get_rsdp() == NULL) return NULL; // RSDP not found, return straight away
	if(acpi_rsdp->revision == 0) {
		if(acpi_rsdt != NULL) return (void*) acpi_rsdt;
		acpi_rsdt = (acpi_rsdt_t*) acpi_rsdp->rsdt_address;
		return (void*) acpi_rsdp->rsdt_address;
	} else {
		if(acpi_xsdt != NULL) return (void*) acpi_xsdt;
		acpi_xsdt = (acpi_xsdt_t*) (acpi_rsdp->xsdt_address & 0xFFFFFFFF);
		return (void*) (acpi_rsdp->xsdt_address & 0xFFFFFFFF);
	}
}

static void *acpi_find_sdt(const uint8_t *signature) {
	if(acpi_get_rsdt() == NULL) return NULL; // RSDT not found, return straight away
	if(acpi_rsdp->revision == 0) {
		for(uint32_t i = 0; i < ((acpi_rsdt->header.length - sizeof(acpi_rsdt->header) / 4)); i++) {
			struct acpi_sdt_header *header = (struct acpi_sdt_header*) acpi_rsdt->sdt_pointer[i];
			if(!strncmp(header->signature, signature, 4)) return (void*) header; 
		}
		return NULL;
	} else {
		for(uint32_t i = 0; i < ((acpi_xsdt->header.length - sizeof(acpi_xsdt->header) / 8)); i++) {
			struct acpi_sdt_header *header = (struct acpi_sdt_header*) acpi_xsdt->sdt_pointer[i];
			if(!strncmp(header->signature, signature, 4)) return (void*) header; 
		}
		return NULL;
	}
	return NULL;
}

static acpi_fadt_t *acpi_get_fadt(void) {
	if(acpi_fadt != NULL) return acpi_fadt; // we already got the FADT pointer
	acpi_fadt = (acpi_fadt_t*) acpi_find_sdt("FACP");
	return acpi_fadt;
}

uint16_t SLP_TYPa = 0, SLP_TYPb = 0, SLP_EN = 0, SCI_EN = 0;

int8_t acpi_init(void) {
	paging86_disable(); // temporarily disable paging
	if(acpi_get_fadt() == NULL) {
		/* something is broken, return straight away */
		printf("[ERROR] acpi_init(): Cannot find FADT, returning with error code -1");
		return -1; 
	}
	/* enable ACPI */
	outb(acpi_fadt->smi_cmdport, acpi_fadt->acpi_enable);
	while(!(inw(acpi_fadt->pm1a_ctrl_blk) & 1)); // wait until ACPI is enabled
	printf("[DEBUG] acpi_init(): switched to ACPI mode\n");
	/* 
		parse FADT to get all the stuff we need for shutdown
		code from https://forum.osdev.org/viewtopic.php?t=16990
	*/
	struct acpi_sdt_header *dsdt_header = acpi_fadt->dsdt;
	if(!strncmp(dsdt_header->signature, "DSDT", 4)) {
		uint8_t *s5_addr = (uint8_t*) acpi_fadt->dsdt + 36;
		uint32_t dsdt_len = (acpi_fadt->dsdt + 1) - 36;
		while(0 < dsdt_len--) {
			if(!strncmp(s5_addr, "_S5_", 4)) break;
			s5_addr++;
		}
		if(dsdt_len > 0) {
			if((*(s5_addr - 1) == 0x08 || (*(s5_addr - 2) == 0x08 && *(s5_addr - 1) == '\\')) && *(s5_addr + 4) == 0x12) {
				s5_addr += 5;
				s5_addr += ((*s5_addr & 0xC0) >> 6) + 2;
				if(*s5_addr == 0x0A) s5_addr++;
				SLP_TYPa = *(s5_addr) << 10;
				s5_addr++;
				if(*s5_addr == 0x0A) s5_addr++;
				SLP_TYPb = *(s5_addr) << 10;
				printf("[DEBUG] acpi_init(): \\_S5 parsed successfully\n");
				SLP_EN = 1 << 13;
				SCI_EN = 1;
			} else {
				/* AML structure invalid */
			printf("[ERROR] acpi_init(): \\_S5 is invalid, returning with error code -2\n");
			return -2; 
			}
		} else {
			/* \_S5 doesn't exist */
			printf("[ERROR] acpi_init(): \\_S5 does not exist, returning with error code -2\n");
			return -2; 
		}
	} else {
		/* DSDT not found or is invalid */
		printf("[ERROR] acpi_init(): DSDT is either invalid or nonexistant, returning with error code -2\n");
		return -2; 
	}
	paging86_enable(); // re-enable paging
	return 0;
}

int8_t acpi_shutdown(void) {
	if(acpi_get_fadt() == NULL) {
		/* something is broken, return straight away */
		printf("[ERROR] acpi_shutdown(): Cannot find FADT, returning with error code -3");
		return -3; 
	}
	if(SCI_EN != 1) {
		printf("[ERROR] acpi_shutdown(): ACPI shutdown not available due to a previous error, returning with error code -2\n");
		return -2; 
	}
	paging86_disable(); // disable paging for good
	printf("[DEBUG] acpi_shutdown(): sending shutdown command to PM1a control block\n");
	outw(acpi_fadt->pm1a_ctrl_blk, (SLP_TYPa | SLP_EN));
	if(acpi_fadt->pm1b_ctrl_blk != 0) {
		printf("[DEBUG] acpi_shutdown(): sending shutdown command to PM1b control block\n");
		outw(acpi_fadt->pm1b_ctrl_blk, (SLP_TYPb | SLP_EN));
	}
	paging86_enable(); // re-enable paging because shutdown failed
	printf("[ERROR] acpi_shutdown(): shutdown failed, returning with error code -1\n");
	return -1;
}