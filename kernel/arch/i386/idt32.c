/* 
	idt32.c - 32-bit IDT library
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
	This file contains functions that can only be used on x86
	machines and therefore is i386-specific.
*/

#include <kernel/idt32.h>
#include <string.h>
#include <stdio.h>

void idt32_entry(uint8_t vector, uint16_t selector, uint32_t handler, uint8_t attribs) {
	idt32_gates[vector].offset_low = (uint16_t) (handler & 0xFFFF);
	idt32_gates[vector].selector = selector;
	idt32_gates[vector].zero = 0;
	idt32_gates[vector].type_attribs = attribs;
	idt32_gates[vector].offset_high = (uint16_t) (handler >> 16);
}

extern void idt32_dummy();

extern void exception_DE();
extern void exception_DB();
extern void exception_NMI();
extern void exception_BP();
extern void exception_OF();
extern void exception_BR();
extern void exception_UD();
extern void exception_NM();
extern void exception_DF();
extern void exception_CSO();
extern void exception_TS();
extern void exception_NP();
extern void exception_SS();
extern void exception_GP();
extern void exception_PF();
extern void exception_MF();
extern void exception_AC();
extern void exception_MC();
extern void exception_XF();
extern void exception_VE();
extern void exception_SX();

void idt32_init(void) {
	/* set up idt register */
	idtr.limit = (sizeof(idt32_gate) * IDT32_ENTRIES) - 1;
	idtr.base = (uint32_t) &idt32_gates;
	
	for(uint16_t i = 0; i < 256; i++) idt32_entry(i, 0x08, (uint32_t) idt32_dummy, 0x8E); // fill IDT vectors with dummy handlers
	
	__asm__ __volatile__("lidtl (%0)" : : "r" (&idtr)); // load idtr
	
	/* set up exception handlers */
	idt32_entry(0, 0x08, (uint32_t) exception_DE, 0x8E);
	idt32_entry(1, 0x08, (uint32_t) exception_DB, 0x8E);
	idt32_entry(2, 0x08, (uint32_t) exception_NMI, 0x8E);
	idt32_entry(3, 0x08, (uint32_t) exception_BP, 0x8E);
	idt32_entry(4, 0x08, (uint32_t) exception_OF, 0x8E);
	idt32_entry(5, 0x08, (uint32_t) exception_BR, 0x8E);
	idt32_entry(6, 0x08, (uint32_t) exception_UD, 0x8E);
	idt32_entry(7, 0x08, (uint32_t) exception_NM, 0x8E);
	idt32_entry(8, 0x08, (uint32_t) exception_DF, 0x8E);
	idt32_entry(9, 0x08, (uint32_t) exception_CSO, 0x8E);
	idt32_entry(10, 0x08, (uint32_t) exception_TS, 0x8E);
	idt32_entry(11, 0x08, (uint32_t) exception_NP, 0x8E);
	idt32_entry(12, 0x08, (uint32_t) exception_SS, 0x8E);
	idt32_entry(13, 0x08, (uint32_t) exception_GP, 0x8E);
	idt32_entry(14, 0x08, (uint32_t) exception_PF, 0x8E);
	idt32_entry(16, 0x08, (uint32_t) exception_MF, 0x8E);
	idt32_entry(17, 0x08, (uint32_t) exception_AC, 0x8E);
	idt32_entry(18, 0x08, (uint32_t) exception_MC, 0x8E);
	idt32_entry(19, 0x08, (uint32_t) exception_XF, 0x8E);
	idt32_entry(20, 0x08, (uint32_t) exception_VE, 0x8E);
	idt32_entry(30, 0x08, (uint32_t) exception_SX, 0x8E);
}

/* exception handler */
typedef struct __attribute__ ((packed)) {
	uint32_t ds;
	uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;
	uint32_t vector, code;
	uint32_t eip, cs, eflags, user_esp, ss;
} exception_regs_t;

void exception_handler(exception_regs_t regs) {
	char *msgs[] = {
		"Division by zero", 
		"Debug", 
		"Non-maskable interrupt", 
		"Breakpoint", 
		"Overflow", 
		"Bound range exceeded", 
		"Invalid opcode", 
		"Device not available", 
		"Double fault", 
		"Coprocessor segment overrun", 
		"Invalid TSS", 
		"Segment not present", 
		"Stack segment fault", 
		"General protection fault", 
		"Page fault", 
		"Reserved", 
		"x87 floating point exception", 
		"Alignment check", 
		"Machine check", 
		"SIMD floating point exception", 
		"Virtualization exception", 
		"Reserved", 
		"Reserved", 
		"Reserved", 
		"Reserved", 
		"Reserved", 
		"Reserved", 
		"Reserved", 
		"Reserved", 
		"Reserved", 
		"Security exception", 
		"Reserved"
	};
	
	printf("[EXCEPTION] %s (0x%X), error code = 0x%X\n", msgs[regs.vector], regs.vector, regs.code);
	
}