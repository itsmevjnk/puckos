/*
	i8259.c - Intel 8259 Programmable Interrupt Controller driver
	<C> Copyright 2018 Weedboi6969@RTEA
*/

#include <kernel/i8259.h>
#include <kernel/inlineasm.h>
#include <kernel/idt32.h>
#include <stdio.h>

#define PIC1							0x20
#define PIC2							0xA0
#define PIC1_COMMAND					PIC1
#define PIC1_DATA						(PIC1 + 1)
#define PIC2_COMMAND					PIC2
#define PIC2_DATA						(PIC2 + 1)

#define PIC_EOI							0x20
void pic_eoi(uint8_t irq) {
	if(irq >= 8) outb(PIC2_COMMAND, PIC_EOI);
	outb(PIC1_COMMAND, PIC_EOI);
}

extern void pic1_isr();
extern void pic2_isr();

#define ICW1_ICW4						0x01
#define ICW1_SINGLE						0x02
#define ICW1_INTERVAL4					0x04
#define ICW1_LEVEL						0x08
#define ICW1_INIT						0x10
#define ICW4_8086						0x01
#define ICW4_AUTO						0x02
#define ICW4_BUF_SLAVE					0x08
#define ICW4_BUF_MASTER					0x0C
#define ICW4_SFNM						0x10
void pic_init(uint8_t offset1, uint8_t offset2) {
	/* backup the masks */
	uint8_t mask1 = inb(PIC1_DATA);
	uint8_t mask2 = inb(PIC2_DATA);
	printf("[DEBUG] pic_init(): master PIC mask = 0x%X ; slave PIC mask = 0x%X\n", mask1, mask2);
	/* initialize both PICs */
	outb(PIC1_COMMAND, ICW1_INIT | ICW1_ICW4); io_wait();
	outb(PIC2_COMMAND, ICW1_INIT | ICW1_ICW4); io_wait();
	/* set offset */
	outb(PIC1_DATA, offset1); io_wait();
	outb(PIC2_DATA, offset2); io_wait();
	/* configure PIC 1 and PIC 2 */
	outb(PIC1_DATA, 4); io_wait(); // there is a slave PIC daisy chained to the master PIC
	outb(PIC2_DATA, 2); io_wait(); // tell the slave PIC its cascade identity
	/* send ICW4_8086 to both PICs */
	outb(PIC1_DATA, ICW4_8086); io_wait();
	outb(PIC2_DATA, ICW4_8086); io_wait();
	/* restore the masks */
	outb(PIC1_DATA, mask1);
	outb(PIC2_DATA, mask2);
	/* load interrupt handlers */
	for(uint8_t i = 0; i < 8; i++) {
		idt32_entry(offset1 + i, 0x08, (uint32_t) pic1_isr, 0x8E);
		idt32_entry(offset2 + i, 0x08, (uint32_t) pic2_isr, 0x8E);
	}
}

void pic_mask(uint8_t irq) {
	uint8_t port;
	if(irq < 8) port = PIC1_DATA;
	else {
		port = PIC2_DATA;
		irq -= 8;
	}
	outb(port, inb(port) | (1 << irq));
}

void pic_unmask(uint8_t irq) {
	uint8_t port;
	if(irq < 8) port = PIC1_DATA;
	else {
		port = PIC2_DATA;
		irq -= 8;
	}
	outb(port, inb(port) & ~(1 << irq));
}