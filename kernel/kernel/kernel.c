/* 
	kernel.c - Kernel source code
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
*/


#include "kernel.h"

/* i386-specific */
mb_header_t *multiboot_header;
void kernel_init_i386(uint32_t mb) {
	multiboot_header = mb; // store Multiboot header for future use
	gdt32_init();
	idt32_init();
	paging86_init();
	kernel_main(ARCH_I386); // pass control to kernel_main where the main events happen
}

void kernel_main(uint8_t arch) {
	srand(1245); // number from random.org, thanks
	// uart_init(0, 115200); // initialize UART
	video_init(1024, 768); // initialize video
	video_setfont(&unicode_8x8); // set video font
	/* initialize TTY */
	tty_setbg(rgb(0, 0, 0));
	tty_setfg(rgb(255, 255, 255));
	tty_init();
	tty_clear();
	
	/* print out information */
	printf("The Puck Operating System Kernel\nKernel architecture: ");
	switch(arch) {
		/* display architecture information */
		case ARCH_I386: printf("i386\n"); break;
		case ARCH_X86_64: printf("amd64\n"); break;
	}
	printf("<C> Copyright 2018 Weedboi6969@Real Time Engineering Associates\n"); 
	
	if(arch == ARCH_I386) {
		printf("[INFO ] kernel_main(): initializing ACPI\n");
		int8_t res = acpi_init();
		if(res != 0) {
			printf("[FATAL] kernel_main(): ACPI failed to initialize (error code %i)\n", res);
			abort();
		} else printf("[INFO ] kernel_main(): ACPI initialized successfully\n");
		printf("[INFO ] kernel_main(): initializing PIC\n");
		pic_init(0x20, 0x28);
		printf("[INFO ] kernel_main(): PIC initialized\n");
		printf("[INFO ] kernel_main(): initializing PS/2 controller\n");
		res = i8042_init();
		if(res != 0) {
			printf("[FATAL] kernel_main(): PS/2 controller failed to initialize (error code %i)\n", res);
			abort();
		} else printf("[INFO ] kernel_main(): PS/2 controller initialized successfully\n");
	}
	
	/* put whatever stuff we want in here */
	while(1) {
		
	}
}