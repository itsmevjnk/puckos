/*
	multiboot.h - Multiboot header parser
	<C> Copyright 2018 Weedboi6969@RTEA
*/

#ifndef KERNEL_MULTIBOOT_H
#define KERNEL_MULTIBOOT_H

#include <stddef.h>
#include <stdint.h>

typedef struct __attribute__ ((packed)) {
	/* flags that determine whether the following fields are valid */
	uint32_t flags;
	/* memory size in Kbytes, valid if bit 0 of flags is set */
	uint32_t mem_lower; // probably 640K
	uint32_t mem_upper;
	/* boot device information, valid if bit 1 of flags is set */
	uint32_t boot_device;
	/* command line pointer, valid if bit 2 of flags is set */
	uint32_t cmdline;
	/* modules information, valid if bit 3 of flags is set */
	uint32_t mods_count;
	uint32_t mods_addr;
	/* symbols, valid if ether bit 4 or bit 5 of flags is set */
	uint8_t syms[12];
	/* memory map information, valid if bit 6 of flags is set */
	uint32_t mmap_length;
	uint32_t mmap_addr;
	/* drives information, valid if bit 7 of flags is set */
	uint32_t drives_length;
	uint32_t drives_addr;
	/* ROM configuration table pointer, valid if bit 8 of flags is set */
	uint32_t config_table;
	/* bootloader name pointer, valid if bit 9 of flags is set */
	uint32_t boot_loader_name;
	/* APM table pointer, valid if bit 10 of flags is set */
	uint32_t apm_table;
	/* VESA BIOS Extension (VBE) information, valid if bit 11 of flags is set */
	uint32_t vbe_control_info;
	uint32_t vbe_mode_info;
	uint16_t vbe_mode;
	uint16_t vbe_interface_seg;
	uint16_t vbe_interface_off;
	uint16_t vbe_interface_len;
	/* VBE framebuffer information, valid if bit 12 of flags is set */
	uint64_t framebuffer_addr;
	uint32_t framebuffer_pitch;
	uint32_t framebuffer_width;
	uint32_t framebuffer_height;
	uint8_t framebuffer_bpp;
	uint8_t framebuffer_type;
	uint8_t color_info[6];
} mb_header_t;

uint64_t mb_get_memsize(void);
void mb_get_bootdev(uint8_t *drive, uint8_t *p1, uint8_t *p2, uint8_t *p3);

#endif