/* 
	int32.h - Protected Mode BIOS Call Functionailty
	Copyright 2009 Napalm@rohitab.com.
	This file contains declaration to a function that can be used
	to call BIOS interrupts in protected mode and therefore is
	i386-specific.
*/

#ifndef KERNEL_INT32_H
#define KERNEL_INT32_H

#include <stddef.h>
#include <stdint.h>

/* int32 registers structure */
typedef struct __attribute__ ((packed)) {
	uint16_t di, si, bp, sp, bx, dx, cx, ax;
	uint16_t gs, fs, es, ds, eflags;
} int32_regs_t;

extern void int32(uint8_t intnum, int32_regs_t *regs);

#endif