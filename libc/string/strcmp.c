#include <string.h>

int strcmp(const char* str1, const char* str2) {
	const uint8_t *p1 = (const uint8_t *) str1;
	const uint8_t *p2 = (const uint8_t *) str2;
	uint8_t c1, c2;
	do {
		c1 = (uint8_t) *p1++;
		c2 = (uint8_t) *p2++;
		if (c1 == '\0') return c1 - c2;
	} while (c1 == c2);
	return c1 - c2;
}